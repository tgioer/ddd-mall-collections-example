package com.mallfoundry.catalog.rest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopCategoryRequest {
    private String name;
}
