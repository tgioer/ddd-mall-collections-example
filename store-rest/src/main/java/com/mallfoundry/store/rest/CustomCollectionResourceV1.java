package com.mallfoundry.store.rest;

import com.mallfoundry.store.CustomCollection;
import com.mallfoundry.store.CustomCollectionService;
import com.mallfoundry.store.StoreId;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class CustomCollectionResourceV1 {

    private final CustomCollectionService customCollectionService;

    public CustomCollectionResourceV1(CustomCollectionService customCollectionService) {
        this.customCollectionService = customCollectionService;
    }

    @PostMapping("/stores/{store_id}/custom_collections")
    public CustomCollection createTopCollection(
            @PathVariable("store_id") String storeId,
            @RequestBody TopCustomCollectionRequest request) {
        return this.customCollectionService.createTopCollection(new StoreId(storeId), request.getName());
    }

    @PostMapping("/stores/{store_id}/custom_collections/{collection_id}/children")
    public CustomCollection addChildCollection(
            @PathVariable("store_id") final String storeId,
            @PathVariable("collection_id") Integer collectionId,
            @RequestBody TopCustomCollectionRequest request) {
        Assert.notNull(storeId, "the store id argument must be null");
        return this.customCollectionService.addChildCollection(collectionId, request.getName());
    }

    @DeleteMapping("/stores/{store_id}/custom_collections/{collection_id}")
    public void deleteCollection(
            @PathVariable("store_id") final String storeId,
            @PathVariable("collection_id") Integer collectionId) {
        Assert.notNull(storeId, "the store id argument must be null");
        this.customCollectionService.deleteCollection(collectionId);
    }

    @GetMapping("/stores/{store_id}/custom_collections")
    public List<CustomCollection> getCollections(
            @PathVariable("store_id") String storeId) {
        return this.customCollectionService.getAllCollections(new StoreId(storeId));
    }

}
