package com.mallfoundry.store.rest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopCustomCollectionRequest {

    private String name;
}
