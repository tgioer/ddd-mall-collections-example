package com.mallfoundry.store;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class CustomCollectionTests {

    @Autowired
    private CustomCollectionService customCollectionService;

    @Autowired
    private CustomCollectionRepository customCollectionRepository;

    @Rollback(false)
    @Transactional
    @Test
    public void testAddCreateCollection() {
        CustomCollection collection = customCollectionService.createTopCollection(new StoreId("mall-foundry"), "top collection 1");
        collection.addChildCollection(collection.createChildCollection("child collection 1"));
        collection.addChildCollection(collection.createChildCollection("child collection 2"));
    }


    @Transactional
    @Rollback
    @Test
    public void testAddCollectionUseDataModel() {
        ChildCustomCollection collection = new ChildCustomCollection();
        collection.setStoreId(new StoreId("mall-foundry"));
        collection.setParent(null); // collection.setParentId("parent id");
        collection.setName("data model collection");
        customCollectionRepository.save(collection);
    }

    @Transactional
    @Rollback
    @Test
    public void testAddCollectionUseDomainModel() {
        CustomCollection collection = customCollectionRepository.findById(1);
        collection.addChildCollection(new ChildCustomCollection("child custom collection"));
    }


    @Transactional
    @Rollback(false)
    @Test
    public void testRemoveOneCollection() {
        CustomCollection collection = this.customCollectionService.getCollection(20);
        CustomCollection child = collection.getChildren().get(0);
        child.setName("child collection 2");
        collection.clearChildCollections();
    }

    @Test
    @Transactional
    public void testFindOneCollection() {
        CustomCollection collection = this.customCollectionService.getCollection(1);
        System.out.println(collection);
    }

    @Test
    public void testFindStoredCollections() {
        List<CustomCollection> collections = this.customCollectionService.getAllCollections(new StoreId("store id"));
        System.out.println(collections);
    }
}
