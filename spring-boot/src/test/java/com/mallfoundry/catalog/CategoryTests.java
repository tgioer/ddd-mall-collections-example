package com.mallfoundry.catalog;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class CategoryTests {

    @Autowired
    private CategoryService categoryService;

    @Transactional
    @Rollback(false)
    @Test
    public void testCreateCategory() {
        Category category = this.categoryService.createTopCategory("top category 3");
        System.out.println(category);
    }

    @Transactional
    @Rollback(false)
    @Test
    public void testAddChildCategory() {
        TopCategory category = this.categoryService.getCategory(6);
        category.addChildCategory(category.createChildCategory("child 1"));
        category.addChildCategory(category.createChildCategory("child 2"));
        category.addChildCategory(category.createChildCategory("child 3"));
    }

    @Transactional
    @Rollback(false)
    @Test
    public void testGetTopCategory() {
        Category category = this.categoryService.getCategory(6);
        System.out.println(category);
    }

}
