package com.mallfoundry.store;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Embeddable
public class StoreId implements Serializable {

    @Column(name = "store_id_")
    private String id;

    public StoreId(final String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoreId storeId = (StoreId) o;
        return Objects.equals(id, storeId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
