package com.mallfoundry.store;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@DiscriminatorValue("child_custom_collection")
public class ChildCustomCollection extends CustomCollection {

    @JsonIgnore
    @ManyToOne
    private CustomCollection parent;

    public ChildCustomCollection(String name) {
        super(name);
    }

    public ChildCustomCollection(CustomCollection parent, String name) {
        super(parent.getStoreId(), name);
        this.setParent(parent);
    }

    @Embedded
    @Override
    public StoreId getStoreId() {
        return Objects.nonNull(this.getParent()) ? this.getParent().getStoreId() : super.getStoreId();
    }
}
