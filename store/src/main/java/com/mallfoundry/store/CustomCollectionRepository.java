package com.mallfoundry.store;

import java.util.List;

public interface CustomCollectionRepository {

    CustomCollection findById(Integer id);

    <S extends CustomCollection> S save(S collection);

    List<CustomCollection> findAll(StoreId storeId);

    void deleteById(Integer id);
}
