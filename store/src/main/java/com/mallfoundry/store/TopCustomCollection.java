package com.mallfoundry.store;


import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@NoArgsConstructor
@Entity
@DiscriminatorValue("top_custom_collection")
public class TopCustomCollection extends CustomCollection {

    public TopCustomCollection(StoreId storeId, String name) {
        super(storeId, name);
    }
}
