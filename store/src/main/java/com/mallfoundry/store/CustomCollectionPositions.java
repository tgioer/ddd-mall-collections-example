package com.mallfoundry.store;

import java.util.Collections;
import java.util.List;

public abstract class CustomCollectionPositions {

    public static void sort(List<? extends CustomCollection> collections) {
        Collections.sort(collections);
        int i = 0;
        for (CustomCollection category : collections) {
            category.setPosition(i++);
        }
    }
}
