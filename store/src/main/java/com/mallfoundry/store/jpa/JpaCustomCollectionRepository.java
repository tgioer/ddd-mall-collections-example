package com.mallfoundry.store.jpa;

import com.mallfoundry.store.CustomCollection;
import com.mallfoundry.store.CustomCollectionRepository;
import com.mallfoundry.store.StoreId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaCustomCollectionRepository
        extends CustomCollectionRepository, JpaRepository<CustomCollection, String> {

    @Query("from CustomCollection where storeId = ?1 and parent is null")
    @Override
    List<CustomCollection> findAll(StoreId storeId);
}
